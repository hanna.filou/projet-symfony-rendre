<?php

namespace App\Form;

use App\Entity\Matiere;
use App\Entity\Intervenant;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
class IntervenantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('age')
            ->add('subjects', EntityType::class, [
                'class' => Matiere::class,
                'query_builder' => function (MatiereRepository $mr) use ($options){
                    $intervenantId= $options['data']->getId();
                    return $mr->createQueryBuilder('m')
                        ->leftJoin('m.intervenant', 'intervenant')
                        ->where('intervenant is null')
                        ->orWhere('intervenant.id = :intervenantId')
                        ->setParameter('intervenantId', $intervenantId)
                    ;
                },
                'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Intervenant::class,
        ]);
    }
}
